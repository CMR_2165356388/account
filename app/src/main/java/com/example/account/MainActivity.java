package com.example.account;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.account.DataBase.AccountData;
import com.example.account.DataBase.AccountDataBase;
import com.example.account.DataBase.AccountDataDao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    String s[]={"一月","二月","三月","四月"};
    TextView tv_month_in,tv_month_out,tv_day_in,tv_day_out;
    ListView today_ls;
    MainLsAdapter mainLsAdapter;

    private AccountDataBase accountDataBase;
    private AccountDataDao accountDataDao;
    private List<AccountData> lst=new ArrayList<>();
    private List<AccountData> today=new ArrayList<>();
    private int ii;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        accountDataBase=AccountDataBase.getDatabase(this);
        accountDataDao=accountDataBase.getDataDao();
        initviews();
        loadData();
        setOn();
    }

    private void setOn() {
        today_ls.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                ii=0;
                final String []s={"查看修改本记录","删除本记录"};
                AlertDialog.Builder DanItem = new AlertDialog.Builder(MainActivity.this);
                DanItem.setTitle("选择想要进行的操作");
                DanItem.setSingleChoiceItems(s, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ii =which;
                    }
                });
                DanItem.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        AccountData now=today.get(i);
                        if(ii==0){
                            //查看
                            Intent intent=new Intent(MainActivity.this,RecordActivity.class);
                            String t= String.valueOf(now.getId());
                            intent.putExtra("id",t);
                            System.out.println("选择对象的id是"+t);
                            intent.putExtra("type","update");
                            startActivity(intent);
                        }
                        else{//删除
                            accountDataDao.DeleteById(now.getId());
                            loadData();
                        }
                    }
                });
                DanItem.create().show();
                return true;
            }
        });
    }

    private void loadData() {
        List<AccountData> lst=accountDataDao.findAll();
        today.clear();
        Date date=new Date();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
        String nowtime=simpleDateFormat.format(date);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH)+1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        //收入1 支出0
        float in_day=accountDataDao.qaskMoney(1,year,month,day);
        float out_day=accountDataDao.qaskMoney(0,year,month,day);
        float in_sum=accountDataDao.qaskMoneySum(1);
        float out_sum=accountDataDao.qaskMoneySum(0);
        today=accountDataDao.qask_by_ymd(day,month,year);
        mainLsAdapter=new MainLsAdapter(this,today);
        today_ls.setAdapter(mainLsAdapter);
        for(AccountData tt:today) System.out.println(tt);
        tv_day_in.setText("+￥"+in_day+"元");
        tv_day_out.setText("-￥"+out_day+"元");
        tv_month_in.setText("￥"+in_sum+"元");
        tv_month_out.setText("￥"+out_sum+"元");
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
    }

    private void initviews() {
        tv_day_in=findViewById(R.id.main_today_in);
        tv_day_out=findViewById(R.id.main_today_out);
        tv_month_in=findViewById(R.id.main_tv_month_in_money);
        tv_month_out=findViewById(R.id.main_tv_month_out_money);
        today_ls=findViewById(R.id.main_lv);
    }

    public void main_add(View view) {
        Intent intent=new Intent(this,RecordActivity.class);
        intent.putExtra("type","add");
        startActivity(intent);
    }

    public void main_more(View view) {
        Intent intent=new Intent(this,MoreActivity.class);
        startActivity(intent);
    }
}