package com.example.account;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.account.DataBase.AccountData;
import com.example.account.DataBase.AccountDataBase;
import com.example.account.DataBase.AccountDataDao;

import java.lang.reflect.Field;
import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HistoryActivity extends AppCompatActivity implements View.OnClickListener{
    ListView lv;
    private int year,month;
    ImageView his_back,his_rili;
    private AccountDataBase accountDataBase;
    private AccountDataDao accountDataDao;
    private MainLsAdapter adapter;
    private List<AccountData> lst=new ArrayList<>();
    TextView tv_time,tv_in,tv_out;
    private float sum_in,sum_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        lv=findViewById(R.id.his_ls);
        his_back=findViewById(R.id.his_back);
        his_rili=findViewById(R.id.his_rili);
        tv_time=findViewById(R.id.his_tv);
        tv_in=findViewById(R.id.his_month_in);
        tv_out=findViewById(R.id.his_month_out);
        his_back.setOnClickListener(this);
        his_rili.setOnClickListener(this);
        getData();
        accountDataBase=AccountDataBase.getDatabase(this);
        accountDataDao=accountDataBase.getDataDao();
        ShowData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ShowData();
    }

    private void ShowData() {
        lst=accountDataDao.qaskMoneyByMonth(month,year);
        adapter=new MainLsAdapter(this,lst);
        //收入1 支出0
        sum_in=accountDataDao.query_Money_by_month_type(1,month,year);
        sum_out=accountDataDao.query_Money_by_month_type(0,month,year);
        tv_in.setText("+￥"+sum_in+"元");
        tv_out.setText("-￥"+sum_out+"元");
        lv.setAdapter(adapter);
        tv_time.setText(year+"年"+month+"月");
    }


    private void getData() {
        Date date=new Date();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
        String nowtime=simpleDateFormat.format(date);
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.his_back:
                finish();
                break;
            case R.id.his_rili:
                showRili();
                break;
        }
    }

    private void showRili() {
        System.out.println("点击了日历");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = View.inflate(this, R.layout.dialog_his, null);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.date_picker_his);
        builder.setView(view);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), null);
        hideDay(datePicker);
        builder.setPositiveButton("确  定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                year=datePicker.getYear();
                month=datePicker.getMonth()+1;
                ShowData();
                System.out.println(year+"年"+month+"月");
                dialog.cancel();//取消dialog
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }
    private void hideDay(DatePicker mDatePicker) {
        try {
            /* android5.0以上 路径都发生变化了 */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
                if (daySpinnerId != 0) {
                    View daySpinner = mDatePicker.findViewById(daySpinnerId);
                    if (daySpinner != null) {
                        daySpinner.setVisibility(View.GONE);//找到天所在的控件设置为不可见
                    }
                }
            } else {
                Field[] datePickerfFields = mDatePicker.getClass().getDeclaredFields();
                for (Field datePickerField : datePickerfFields) {
                    if ("mDaySpinner".equals(datePickerField.getName()) || ("mDayPicker").equals(datePickerField.getName())) {
                        datePickerField.setAccessible(true);
                        Object dayPicker = new Object();
                        try {
                            dayPicker = datePickerField.get(mDatePicker);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        ((View) dayPicker).setVisibility(View.GONE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}