package com.example.account.Utils;

import android.app.Application;

import com.example.account.DataBase.Manager_type;

public class UniteApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // 初始化数据库
        Manager_type.init(getApplicationContext());
    }
}
