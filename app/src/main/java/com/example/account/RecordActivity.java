package com.example.account;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;

import com.example.account.DataBase.AccountData;
import com.example.account.DataBase.AccountDataBase;
import com.example.account.DataBase.AccountDataDao;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class RecordActivity extends AppCompatActivity {
    TabLayout tableLayout;
    ViewPager viewPager;
    List<Fragment> fragmentList=new ArrayList<>();
    Intent intent;
    String model;
    int lasid;
    private AccountDataBase accountDataBase;
    private AccountDataDao accountDataDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        tableLayout=findViewById(R.id.record_tabs);//查找控件
        viewPager=findViewById(R.id.record_vp);
        intent=getIntent();
        model=intent.getStringExtra("type");
        accountDataBase= AccountDataBase.getDatabase(this);
        accountDataDao=accountDataBase.getDataDao();
        initViews();
    }

    private void initViews() {
        if(model.equals("add")){
            OutFragment of=new OutFragment();
            InFragment inf=new InFragment();
            fragmentList.add(of);
            fragmentList.add(inf);
            String []titles={"支出","收入"};
            viewPager.setAdapter(new RecordFragAdapter(getSupportFragmentManager(),fragmentList,titles));
            tableLayout.setupWithViewPager(viewPager);
        }
        else{
            lasid= Integer.parseInt(intent.getStringExtra("id"));
            AccountData accountData=accountDataDao.FindById(lasid);
            int now_type=accountData.getType();
            //收入1 支出0
            if(now_type==1){
                InFragment inf=new InFragment();
                fragmentList.add(inf);
                String []titles={"收入"};
                viewPager.setAdapter(new RecordFragAdapter(getSupportFragmentManager(),fragmentList,titles));
                tableLayout.setupWithViewPager(viewPager);
            }
            else{
                OutFragment of=new OutFragment();
                fragmentList.add(of);
                String []titles={"支出"};
                viewPager.setAdapter(new RecordFragAdapter(getSupportFragmentManager(),fragmentList,titles));
                tableLayout.setupWithViewPager(viewPager);
            }
        }
    }

    public void record_back(View view) {
        System.out.println("结束");
        finish();
    }
}