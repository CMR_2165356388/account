package com.example.account;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.account.DataBase.AccountDataBase;
import com.example.account.DataBase.AccountDataDao;

public class MoreActivity extends AppCompatActivity {
    private static AccountDataBase accountDataBase;
    private static AccountDataDao accountDataDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);
        accountDataBase=AccountDataBase.getDatabase(this);
        accountDataDao=accountDataBase.getDataDao();
    }

    public void more_share(View view) {
        Intent intent=new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,"快来看看我分享给你的记账软件吧~下载地址为www.yiui.xyz");
        startActivity(Intent.createChooser(intent,"RichAfter"));
    }

    public void more_details(View view) {
        Intent intent=new Intent(this,HistoryActivity.class);
        startActivity(intent);
    }

    public void more_delete(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MoreActivity.this);
        builder.setIcon(R.drawable.more_delete);
        builder.setTitle("温馨提示");
        builder.setMessage("确定要清空数据嘛?");
        builder.setPositiveButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // 点击逻辑
                finish();  // 程序退出
            }
        });

        builder.setNegativeButton("是", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                accountDataDao.deleteAll();
                finish();
            }
        });
        builder.create().show();
    }

    public void more_back(View view) {
        finish();
    }
}