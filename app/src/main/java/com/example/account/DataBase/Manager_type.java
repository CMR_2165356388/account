package com.example.account.DataBase;

import android.content.Context;
import com.example.account.DataBase.TypeDatabase;
import com.example.account.R;

import java.util.ArrayList;
import java.util.List;

public class Manager_type {
    private static TypeDatabase typeDatabase;
    private static TypeDataDao typeDataDao;
    private static AccountDataBase accountDataBase;
    private static AccountDataDao accountDataDao;
    private static List<TypeData>sin=new ArrayList<>();
    private static List<TypeData>sout=new ArrayList<>();
    public static void init(Context applicationContext) {
        typeDatabase=TypeDatabase.getDatabase(applicationContext);
        typeDataDao= (TypeDataDao) typeDatabase.getDataDao();
        accountDataBase=AccountDataBase.getDatabase(applicationContext);
        accountDataDao=accountDataBase.getDataDao();
        //typeDataDao.deleteAll();
     //   accountDataDao.deleteAll();
        sin=typeDataDao.findByKind(-1);
        sout=typeDataDao.findByKind(1);
        if(sin.isEmpty()){
            Inset_in();
            sin=typeDataDao.findByKind(-1);
        }
        if(sout.isEmpty()){
            Inset_out();
            sout=typeDataDao.findByKind(1);
        }
        for(TypeData tt:sin){
            System.out.println(tt);
        }
        for(TypeData tt:sout){
            System.out.println(tt);
        }
    }

    private static void Inset_out() {
        typeDataDao.insertTypeData(new TypeData("餐饮", R.mipmap.it_eat,R.mipmap.ic_eat,1));
        typeDataDao.insertTypeData(new TypeData("衣服", R.mipmap.it_cloth,R.mipmap.ic_cloth,1));
        typeDataDao.insertTypeData(new TypeData("宠物", R.mipmap.it_chongwu,R.mipmap.ic_chongwu,1));
        typeDataDao.insertTypeData(new TypeData("发红包", R.mipmap.it_fahongbao,R.mipmap.ic_fahongbao,1));
        typeDataDao.insertTypeData(new TypeData("儿童", R.mipmap.it_haizi,R.mipmap.ic_haizi,1));
        typeDataDao.insertTypeData(new TypeData("话费网费", R.mipmap.it_huafei,R.mipmap.ic_huafei,1));
        typeDataDao.insertTypeData(new TypeData("交通出行", R.mipmap.it_jiaotong,R.mipmap.ic_jiaotong,1));
        typeDataDao.insertTypeData(new TypeData("零食", R.mipmap.it_lingshi,R.mipmap.ic_lingshi,1));
        typeDataDao.insertTypeData(new TypeData("请客送礼", R.mipmap.it_liwu,R.mipmap.ic_liwu,1));
        typeDataDao.insertTypeData(new TypeData("旅行", R.mipmap.it_lvxing,R.mipmap.ic_lvxing,1));
        typeDataDao.insertTypeData(new TypeData("美妆护肤", R.mipmap.it_meizhuang,R.mipmap.ic_meizhuang,1));
        typeDataDao.insertTypeData(new TypeData("汽油", R.mipmap.it_qiyou,R.mipmap.ic_qiyou,1));
        typeDataDao.insertTypeData(new TypeData("水电煤", R.mipmap.it_shuidianmei,R.mipmap.ic_shuidianmei,1));
        typeDataDao.insertTypeData(new TypeData("电器数码", R.mipmap.it_shuma,R.mipmap.ic_shuma,1));
        typeDataDao.insertTypeData(new TypeData("学习", R.mipmap.it_xuexi,R.mipmap.ic_xuexi,1));
        typeDataDao.insertTypeData(new TypeData("医疗", R.mipmap.it_yiliao,R.mipmap.ic_yiliao,1));
        typeDataDao.insertTypeData(new TypeData("运动", R.mipmap.it_yundong,R.mipmap.ic_yundong,1));
        typeDataDao.insertTypeData(new TypeData("娱乐", R.mipmap.it_yvle,R.mipmap.ic_yvle,1));
        typeDataDao.insertTypeData(new TypeData("住房", R.mipmap.it_zhufang,R.mipmap.ic_zhufang,1));
        typeDataDao.insertTypeData(new TypeData("其他", R.mipmap.it_qita,R.mipmap.ic_qita,1));
    }

    private static void Inset_in() {
       typeDataDao.insertTypeData(new TypeData("工资", R.mipmap.it_gongzi,R.mipmap.ic_gongzi,-1));
       typeDataDao.insertTypeData(new TypeData("红包", R.mipmap.it_hongbao,R.mipmap.ic_hongbao,-1));
        typeDataDao.insertTypeData(new TypeData("生活费", R.mipmap.it_shenghuofei,R.mipmap.ic_shenghuofei,-1));
        typeDataDao.insertTypeData(new TypeData("兼职收入", R.mipmap.it_jianzhi,R.mipmap.ic_jianzhi,-1));
        typeDataDao.insertTypeData(new TypeData("投资", R.mipmap.it_touzi,R.mipmap.ic_touzi,-1));
        typeDataDao.insertTypeData(new TypeData("保险", R.mipmap.it_baoxian,R.mipmap.ic_baoxian,-1));
        typeDataDao.insertTypeData(new TypeData("储蓄罐", R.mipmap.it_chuxuguan,R.mipmap.ic_chuxuguan,-1));
        typeDataDao.insertTypeData(new TypeData("其他", R.mipmap.it_qita,R.mipmap.ic_qita,-1));
    }

}
