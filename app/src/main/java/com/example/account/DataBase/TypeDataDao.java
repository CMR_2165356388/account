package com.example.account.DataBase;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TypeDataDao {
    @Insert
    void insertTypeData(TypeData data);
    @Query("delete from TypeData")
    void deleteAll();
    @Query("select * from TypeData")
    List<TypeData> findAll();
    @Query("select * from TypeData where kind=:pos")
    List<TypeData> findByKind(int pos);
}
