package com.example.account;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class RecordFragAdapter extends FragmentPagerAdapter {
    List<Fragment> fragmentList;
    String []titles;
    public RecordFragAdapter(@NonNull FragmentManager fm,List<Fragment> fragmentList,String []titlestitles) {
        super(fm);
        this.fragmentList=fragmentList;
        this.titles=titlestitles;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
