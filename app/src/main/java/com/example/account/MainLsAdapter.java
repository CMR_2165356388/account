package com.example.account;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.account.DataBase.AccountData;

import java.util.List;

public class MainLsAdapter extends BaseAdapter {
    Context context;
    List<AccountData> lst;

    public MainLsAdapter(Context context, List<AccountData> lst) {
        this.context = context;
        this.lst = lst;
    }

    @Override
    public int getCount() {
        return lst.size();
    }

    @Override
    public Object getItem(int i) {
        return lst.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        AccountData accountData=lst.get(i);
        //想要添加的布局，想要添加到哪个布局上，是否直接添加到第二个布局上
        view= LayoutInflater.from(context).inflate(R.layout.item_main_lv,viewGroup,false);
        ImageView iv=view.findViewById(R.id.item_mainlv_iv);
        TextView tv_name=view.findViewById(R.id.item_mainlv_tv_title);
        TextView tv_bz=view.findViewById(R.id.item_mainlv_tv_bz);
        TextView tv_money=view.findViewById(R.id.item_mainlv_tv_money);
        TextView tv_time=view.findViewById(R.id.item_mainlv_tv_time);
        iv.setImageResource(accountData.getImageid());
        tv_name.setText(accountData.getName());
        tv_bz.setText(accountData.getBz());
        if(accountData.getType()==1)
            tv_money.setText("+￥"+accountData.getMoney()+"元");
        else tv_money.setText("-￥"+accountData.getMoney()+"元");
        tv_time.setText(accountData.getTime());
        return view;
    }
}
